# frozen_string_literal: true
#
require 'spec_helper'

describe 'comment on a group resources' do
  include_context 'integration'

  let(:group_id) { 456 }
  let(:argv) { %W[--source groups --source-id #{group_id} --token #{token}] }

  it 'comments on the issue' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Rule name
              conditions:
                state: open
              actions:
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    stub_api(
      :get,
      "https://gitlab.com/api/v4/groups/#{group_id}/issues",
      query: { per_page: 100, state: 'open' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue[:iid]}/notes",
      body: { body: 'This is my comment for issues' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  it 'comments on the merge request' do
    rule = <<~YAML
      resource_rules:
        merge_requests:
          rules:
            - name: Rule name
              conditions:
                state: open
              actions:
                comment: |
                  This is my comment for \#{resource[:type]}
    YAML

    stub_api(
      :get,
      "https://gitlab.com/api/v4/groups/#{group_id}/merge_requests",
      query: { per_page: 100, state: 'open' },
      headers: { 'PRIVATE-TOKEN' => token }) do
      issues
    end

    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{issue[:iid]}/notes",
      body: { body: 'This is my comment for merge_requests' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end
end
