require_relative 'params_validator'

module Gitlab
  module Triage
    class LimiterValidator < ParamsValidator
      private

      def params_limiter_names
        @parameter_definitions.map do |param|
          param[:name]
        end
      end

      def validate_required_parameters(value)
        raise ArgumentError, "For the limits field, please specify one of: `#{params_limiter_names.join('`, `')}`" unless value.keys.map(&:to_sym).one? { |key| params_limiter_names.include?(key) }
      end
    end
  end
end
